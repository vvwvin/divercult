﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

	[SerializeField] GameObject configParent;

	public void ChangeScene (string sceneToLoad)
	{
		SceneManager.LoadScene (sceneToLoad);
	}

	public void ChangeScene (int sceneToLoad)
	{
		SceneManager.LoadScene (sceneToLoad);
	}

	public void OpenConfig ()
	{
		configParent.SetActive (true);
	}

	public void CloseConfig ()
	{
		configParent.SetActive (false);
	}
}
