using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Divercult.Level1
{
	[Serializable]
	public class Deck
	{
		public DeckCard[] cards;
	}

	[Serializable]
	public class DeckCard
	{
		public int imageId;
		public int descriptionId;
		public string description;
	}
}