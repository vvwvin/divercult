using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace Divercult.Level1
{
	[RequireComponent(typeof(CardController))]
	public class CardOnStack : MonoBehaviour, IPointerDownHandler
	{
		private CardController card = null;
		private bool hasFlipped = false;

		private void Awake()
		{
			card = GetComponent<CardController>();
		}

		private void Start()
		{
			//manter este método mesmo que vazio
			card.transform.localPosition = CardController.STACKPOSITION;
		}

		private void OnEnable()
		{
			card.transform.SetParent(Board.instance.HandParent);
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			CardController cardOnHand = Player.GetCardFromHand(0);

			if (cardOnHand != null && cardOnHand.State == CardState.OnHand)
			{
				Stack.instance.ReturnCardToStack(cardOnHand);
				cardOnHand.GetComponent<CardOnStack>().Hide();
				cardOnHand.transform.DOLocalMove(CardController.STACKPOSITION, CardController.MOVETRANSITIONDURATION)
					.SetEase(Ease.OutBack);
				Player.RemoveFromHand(cardOnHand);
				cardOnHand.TransitionTo(CardState.OnStack);
			}
		}

		public void Reveal(bool moveToBoard = false)
		{
			if (hasFlipped == true)
			{ return; }

			card.BackSide.DOFade(0f, CardController.FLIPTRANSITIONDURATION)
					.OnComplete(new TweenCallback(() =>
					{
						if (moveToBoard)
						{
							MoveToBoard();
							Board.instance.LockFirstCard(card);
						}
						else
						{
							MoveToHand();
						}
					}));
		}

		public void Hide()
		{
			if (hasFlipped == false)
			{ return; }

			card.BackSide.DOFade(1f, CardController.FLIPTRANSITIONDURATION)
				.OnComplete(new TweenCallback(() =>
				{
					hasFlipped = false;
				}));
		}

		private void MoveToHand()
		{
			if (hasFlipped)
				return;

			hasFlipped = true;

			//adiciona carta a mão do player
			Player.AddToHand(card);
			card.TransitionTo(CardState.OnHand);
		}

		private void MoveToBoard()
		{
			if (hasFlipped)
				return;

			hasFlipped = true;

			transform.DOLocalMove(Vector3.zero, CardController.MOVETRANSITIONDURATION);
			card.TransitionTo(CardState.BoardLocked);
		}
	}
}
