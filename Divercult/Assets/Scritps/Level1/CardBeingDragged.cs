using System;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace Divercult.Level1
{
	[RequireComponent(typeof(CardController))]
	public class CardBeingDragged : MonoBehaviour, IDragHandler, IEndDragHandler
	{
		public static event Action<CardController, ColliderSide> OnAnyCardLocked = delegate { };

		private CardController card = null;
		private CardData mock = null;
		private Tween scaleTween = null;

		private void Awake()
		{
			card = GetComponent<CardController>();
			mock = GetComponent<CardData>();
		}

		private void Start()
		{
			//manter este método mesmo que vazio
		}

		private void OnEnable()
		{
			card.transform.SetParent(Board.instance.BoardParent);
		}

		private Collider2D[] contacts = new Collider2D[1];
		public void OnEndDrag(PointerEventData eventData)
		{
			Cursor.visible = true;

			transform.DOScale(Vector3.one, CardController.SCALETRANSITIONDURATION);

			//checar contato do lado da imagem
			int imageSideCollisionCount = card.ImageSideCollider.collider.OverlapCollider(card.Filter, contacts);

			if (imageSideCollisionCount > 0)
			{
				CardCollider collider = contacts[0].GetComponent<CardCollider>();
				if (collider != null && collider.Card.State == CardState.BoardLocked && collider.Type == ColliderType.Description && collider.Side != card.ImageSideCollider.Side && card.ImageSideCollider.Id == collider.Id)
				{
					card.transform.DOMove(collider.transform.position + Vector3.right * CardController.CARDLOCKXOFFSET, CardController.MOVETRANSITIONDURATION);

					card.TransitionTo(CardState.BoardLocked);

					OnAnyCardLocked(card, collider.Side);
					return;
				}
			}

			//checar contato do lado da descrição
			int descriptionSideCollisionCount = card.DescriptionSideCollider.collider.GetContacts(contacts);

			if (descriptionSideCollisionCount > 0)
			{
				CardCollider collider = contacts[0].GetComponent<CardCollider>();
				if (collider != null && collider.Card.State == CardState.BoardLocked && collider.Type == ColliderType.Image && collider.Side != card.DescriptionSideCollider.Side && card.DescriptionSideCollider.Id == collider.Id)
				{
					card.transform.DOMove(collider.transform.position - Vector3.right * CardController.CARDLOCKXOFFSET, CardController.MOVETRANSITIONDURATION);

					card.TransitionTo(CardState.BoardLocked);

					OnAnyCardLocked(card, collider.Side);
					return;
				}
			}

			//se nenhum contato, voltar pra mão
			card.TransitionTo(CardState.OnHand);
		}

		public void OnDrag(PointerEventData eventData)
		{
			transform.localPosition = transform.localPosition + (Vector3)eventData.delta;
		}
	}
}
