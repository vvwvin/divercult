using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Divercult.Level1
{
	public class Stack : MonoBehaviour
	{
		public static Stack instance = null;

		[Header("References")]
		[SerializeField] private CardController cardPrefab;
		[Header("Settings")]
		[SerializeField] private float period = 0.125f;

		private Queue<CardController> stack;
		private List<CardController> allCards;

		private void Awake()
		{
			if (instance == null)
			{ instance = this; }
			if (instance != this)
			{ Destroy(gameObject); }

			CardBeingDragged.OnAnyCardLocked += OnCardLocked;

			stack = new Queue<CardController>(28);
			allCards = new List<CardController>(28);

			GenerateCardStack();
		}

		private void GenerateCardStack()
		{
			Sprite defaultImageSprite = Resources.Load<Sprite>("Level1Images/default");

			Deck deck = null;
			List<DeckCard> deckList = new List<DeckCard>(15);

			using (FileStream stream = new FileStream(Application.dataPath + "/StreamingAssets/XML/level1deck.xml", FileMode.Open))
			{
				XmlSerializer serializer = new XmlSerializer(typeof(Deck));
				deck = serializer.Deserialize(stream) as Deck;
			}

			deckList.AddRange(deck.cards);

			Sprite deckCardImageSprite = null;
			for (int i = deckList.Count - 1; i >= 0; i--)
			{
				DeckCard deckCard = deckList[Random.Range(0, deckList.Count)];

				deckCardImageSprite = Resources.Load<Sprite>("Level1Images/" + deckCard.imageId);
				if (deckCardImageSprite == null)
				{ deckCardImageSprite = defaultImageSprite; }

				CardController card = Instantiate(cardPrefab, transform.localPosition, Quaternion.identity, Board.instance.HandParent);
				card.GetComponent<CardData>().Setup(deckCard, deckCardImageSprite);

				stack.Enqueue(card);
				allCards.Add(card);

				deckCardImageSprite = null;
				deckList.Remove(deckCard);
			}

			Debug.Log(allCards.Count + " cards carregados.");

			StartCoroutine(SeedGame());
		}

		private IEnumerator SeedGame()
		{
			//adiciona 5 cartas para a mão do player
			yield return new WaitForSeconds(period);
			stack.Dequeue().GetComponent<CardOnStack>().Reveal(true);
			yield return new WaitForSeconds(period);

			for (int i = 0; i < 5; i++)
			{
				DrawCard();
				yield return new WaitForSeconds(period);
			}
		}

		private void ResetBoard()
		{
			foreach (CardController c in allCards)
			{ Destroy(c.gameObject); }

			Player.ClearHand();
			stack.Clear();
			allCards.Clear();
			stack = new Queue<CardController>(28);
			allCards = new List<CardController>(28);

			GenerateCardStack();
		}

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.T))
			{ ResetBoard(); }
		}

		private void DrawCard()
		{
			if (stack.Count > 0)
			{ stack.Dequeue().GetComponent<CardOnStack>().Reveal(); }
		}

		public void ReturnCardToStack(CardController card)
		{
			if (card != null && card.State == CardState.OnHand)
			{
				stack.Dequeue().GetComponent<CardOnStack>().Reveal();
				stack.Enqueue(card);
			}
		}

		private void OnCardLocked(CardController card, ColliderSide side)
		{
			DrawCard();
		}

		private void OnDisable()
		{
			CardBeingDragged.OnAnyCardLocked -= OnCardLocked;
		}
	}
}