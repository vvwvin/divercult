﻿using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace Divercult.Level1
{
	public class CameraDrag : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
	{
		[SerializeField] private Camera cam;
		[SerializeField] private float sensibility = 0.025f;

		private Tween moveTween;

		private void Awake()
		{
			if (cam == null)
			{ cam = Camera.main; }

			moveTween = cam.transform.DOMove(cam.transform.position, float.MinValue);
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			if (Input.GetMouseButtonDown(1))
			{
				moveTween.Kill();

				Vector3 targetPosition;
				if (Vector3.Distance(cam.transform.position, Board.instance.LeftmostOpenPosition) > Vector3.Distance(cam.transform.position, Board.instance.RightmostOpenPosition))
				{ targetPosition = Board.instance.LeftmostOpenPosition; }
				else
				{ targetPosition = Board.instance.RightmostOpenPosition; }

				targetPosition.z = cam.transform.position.z;

				cam.transform.DOMove(targetPosition, CardController.MOVETRANSITIONDURATION)
					.SetEase(Ease.InOutQuad);
			}
		}

		public void OnBeginDrag(PointerEventData eventData)
		{
			Cursor.visible = false;
		}

		public void OnDrag(PointerEventData eventData)
		{
			cam.transform.localPosition = cam.transform.localPosition - (Vector3)eventData.delta * sensibility;
		}

		public void OnEndDrag(PointerEventData eventData)
		{
			Cursor.visible = true;
		}
	}
}