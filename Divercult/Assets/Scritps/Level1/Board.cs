using System;
using System.Collections.Generic;
using UnityEngine;

namespace Divercult.Level1
{
    public class Board : MonoBehaviour
    {
        public static Board instance = null;

        public Transform HandParent { get { return handParent; } }
        public Transform BoardParent { get { return boardParent; } }
        public int LeftId { get; private set; }
        public int RightId { get; private set; }
        public Vector3 LeftmostOpenPosition { get { return lockedCards[0].transform.position; } }
        public Vector3 RightmostOpenPosition
        { get { return lockedCards[lockedCards.Count - 1].transform.position; } }
        public int LockedCardCount { get { return lockedCards.Count; } }

        [SerializeField] private Transform handParent;
        [SerializeField] private Transform boardParent;

        private List<CardController> lockedCards = new List<CardController>();

        private void Awake()
        {
            if (instance == null) { instance = this; }
            if (instance != this) { Destroy(gameObject); }

            CardBeingDragged.OnAnyCardLocked += OnCardLocked;
        }

        public void UpdateOpenPositions()
        {
            foreach (CardController card in lockedCards)
            {
                card.ImageSideCollider.gameObject.SetActive(false);
                card.DescriptionSideCollider.gameObject.SetActive(false);
            }

            if (lockedCards[0].ImageSideCollider.Side == ColliderSide.Left)
            {
                lockedCards[0].ImageSideCollider.gameObject.SetActive(true);
                LeftId = lockedCards[0].ImageSideCollider.Id;
            }
            else
            {
                lockedCards[0].DescriptionSideCollider.gameObject.SetActive(true);
                LeftId = lockedCards[0].DescriptionSideCollider.Id;
            }

            if (lockedCards[lockedCards.Count - 1].ImageSideCollider.Side == ColliderSide.Right)
            {
                lockedCards[lockedCards.Count - 1].ImageSideCollider.gameObject.SetActive(true);
                RightId = lockedCards[lockedCards.Count - 1].ImageSideCollider.Id;
            }
            else
            {
                lockedCards[lockedCards.Count - 1].DescriptionSideCollider.gameObject.SetActive(true);
                RightId = lockedCards[lockedCards.Count - 1].DescriptionSideCollider.Id;
            }
        }

        private void OnCardLocked(CardController card, ColliderSide side)
        {
            if (side == ColliderSide.Left)
            { lockedCards.Insert(0, card); }
            else
            { lockedCards.Insert(lockedCards.Count, card); }

            UpdateOpenPositions();

            if (lockedCards.Count >= 15)
            { Debug.LogError("condicao de vitoria atingida"); }
        }

        public void LockFirstCard(CardController card)
        {
            if (lockedCards.Count == 0)
            { lockedCards.Add(card); }
            else
            { Debug.Log("Board.LockFirstCard() chamado com lockedCards.Count > 0"); }

            UpdateOpenPositions();
        }

        private void OnDisable()
        {
            CardBeingDragged.OnAnyCardLocked -= OnCardLocked;
        }
    }
}