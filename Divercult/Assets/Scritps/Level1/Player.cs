using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Divercult.Level1
{
	public static class Player
	{
		public static event Action OnHandChanged = delegate { };

		public static int HandCount { get { return hand.Count; } }

		private static List<CardController> hand = new List<CardController>();

		public static void AddToHand(CardController card)
		{
			if (hand.Contains(card))
			{
				Debug.LogError("Erro: Não deve ser possível ter cartas repetidas na mão.");
				return;
			}

			hand.Add(card);
			OnHandChanged();
		}

		public static void RemoveFromHand(CardController card)
		{
			bool isInHand = hand.Remove(card);

			if (!isInHand)
				Debug.Log("Tentando remover carta que não estava na mão.");
		}

		public static void ClearHand()
		{
			hand.Clear();
		}

		public static CardController GetCardFromHand(int index)
		{
			if (index < 0 || index > hand.Count - 1)
			{ return null; }

			return hand[index];
		}

		public static int? FindIndex(CardController card)
		{
			if (hand.Contains(card))
			{
				for (int i = 0; i < hand.Count; i++)
				{
					if (hand[i] == card)
						return i;
				}
			}

			return null;
		}
	}
}