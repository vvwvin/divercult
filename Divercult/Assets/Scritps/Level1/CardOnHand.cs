using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace Divercult.Level1
{
	[RequireComponent(typeof(CardController))]
	public class CardOnHand : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDragHandler
	{
		private Vector3 targetPosition = Vector3.zero;
		private CardController card = null;

		private void Awake()
		{
			card = GetComponent<CardController>();
		}

		private void Start()
		{
			//manter este método mesmo que vazio
		}

		private Tween moveTween = null;
		private void OnEnable()
		{
			card.transform.SetParent(Board.instance.HandParent);
			Player.OnHandChanged += OnHandChanged;

			UpdateTargetPosition();

			if (moveTween != null)
				moveTween.Kill();

			moveTween = transform.DOLocalMove(targetPosition, CardController.MOVETRANSITIONDURATION);
		}

		private void OnDisable()
		{
			Player.OnHandChanged -= OnHandChanged;
		}

		private Tween scaleTween = null;
		public void OnPointerEnter(PointerEventData eventData)
		{
			if (scaleTween != null)
				scaleTween.Kill();

			scaleTween = transform.DOScale(Vector3.one * CardController.ONMOUSEOVERSCALEFACTOR, CardController.SCALETRANSITIONDURATION);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			if (scaleTween != null)
				scaleTween.Kill();

			scaleTween = transform.DOScale(Vector3.one, CardController.SCALETRANSITIONDURATION);
		}

		public void OnDrag(PointerEventData eventData)
		{
			Cursor.visible = false;
			card.TransitionTo(CardState.BeingDragged);
		}

		private void OnHandChanged()
		{
			UpdateTargetPosition();

			if (moveTween != null)
				moveTween.Kill();

			moveTween = transform.DOLocalMove(targetPosition, CardController.MOVETRANSITIONDURATION);
		}

		private void UpdateTargetPosition()
		{
			if (!enabled)
				return;

			int? position = Player.FindIndex(card);

			if (position != null)
			{
				int hand = Mathf.Clamp(Player.HandCount - 1, 1, int.MaxValue);
				targetPosition = new Vector3
				(
					-CardController.MAXLOCALXPOSITION + (CardController.MAXLOCALXPOSITION * 2f / hand) * (int)position,
					CardController.LOCALYPOSITION,
					0f
				);
			}
			else
			{
				Debug.LogError("position fuckt up");
			}
		}
	}
}
