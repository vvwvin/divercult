﻿using System;
using UnityEngine;

namespace Divercult.Level1
{
	public enum ColliderType { Image, Description }
	public enum ColliderSide { Left, Right }

	public class CardCollider : MonoBehaviour
	{
		public ColliderType Type { get { return type; } }
		public ColliderSide Side { get { return side; } set { side = value; } }
		public new Collider2D collider { get; private set; }
		public CardController Card { get { return card; } }
		public int Id { get { return type == ColliderType.Image ? mock.imageId : mock.descriptionId; } }

		[SerializeField] private ColliderType type;
		[SerializeField] private ColliderSide side;

		private CardController card;
		private CardData mock;

		private void Awake()
		{
			card = GetComponentInParent<CardController>();
			mock = GetComponentInParent<CardData>();
			collider = GetComponent<Collider2D>();
		}
	}
}