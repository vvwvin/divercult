﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu (menuName = "Scriptable/ProfileVariable")]
public class ProfileVariable : ScriptableObject
{
	[Multiline (20)]
	public string reporterText;

	[Multiline (20)]
	public string passaportText;

	[Multiline (20)]
	public string curriculumText;

	[Multiline (20)]
	public string pcOriginCountryText;

	[Multiline (20)]
	public string pcAvailableJobsText;


	private bool status;

	public void Init (string textoReporter, string passaportText, string curriculumText, string pcAvailableJobsText, string pcOriginCountryText)
	{
		this.reporterText = textoReporter;
		this.passaportText = passaportText;
		this.curriculumText = curriculumText;
		this.pcAvailableJobsText = pcAvailableJobsText;
		this.pcOriginCountryText = pcOriginCountryText;
	}

	public string GetReporterText ()
	{
		return reporterText;
	}

	public string GetPassportText ()
	{
		return passaportText;
	}

	public string GetCurriculumText ()
	{
		return curriculumText;
	}

	public string GetPCOriginCountryText ()
	{
		return pcOriginCountryText;
	}

	public string GetPCAvailableJobsText ()
	{
		return pcAvailableJobsText;
	}

	public void SetProfileStatus (int Status)
	{
		if (Status == 0)
			status = false;
		else
			status = true;
	}
}
