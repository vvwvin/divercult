﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu (menuName = "Scriptable/ReportVariable")]
public class ReportVariable : ScriptableObject
{
	[SerializeField]private int[] profileIndex;
	[SerializeField]private string[] profileStatus;
	private int i;

	public void SetProfileIndex (int index)
	{
		profileIndex [index] = index;
	}

	public int GetIndex ()
	{
		return profileIndex.Length;
	}

	public void SetProfileStatus (int index, int decision)
	{
		i = decision;
		if (i == 0) {
			profileStatus [index] = "Negado";
		}
		if (i == 1) {
			profileStatus [index] = "Aprovado";
		}		
	}

	public void DefineLength (int length)
	{
		profileIndex = new int[length];
		profileStatus = new string[length];
	}

	void OnDisable ()
	{
		i = 0;
	}
}
