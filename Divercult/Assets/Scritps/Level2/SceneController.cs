﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{

	[SerializeField]private GameObject uiInitial;
	[SerializeField]private GameObject uiFiles;
	[SerializeField]private GameObject uiPC;
	[SerializeField]private GameObject uiTV;
	[SerializeField]private List<ProfileVariable> profiles2;
	[SerializeField]private ReportVariable report;
	[SerializeField]private NewEnterviewVariable novaEntrevista;
	[SerializeField]private Text reporterText;
	[SerializeField]private Text filesDocText;
	[SerializeField]private Text pcText;

	[SerializeField]private Text inputReporterText;
	[SerializeField]private Text inputPassaportText;
	[SerializeField]private Text inputCurriculumText;
	[SerializeField]private Text inputPcAvailableJobsText;
	[SerializeField]private Text inputPcOriginCountryText;

	[SerializeField] private GameObject canvasGameplay;
	[SerializeField] private GameObject canvasConfig;

    private XMLManager _xmlManager;

	public int i = 0;

	void Awake ()
	{
        _xmlManager = XMLManager.Instance;

        //Carrega o XML e adiciona os perfis salvos na lista de perfis do jogo atual.
        _xmlManager.Load();

        foreach (EntrevistadoEntry entry in _xmlManager.entrevistadosDB.EntrevistadoList)
        {
            ProfileVariable profile = ScriptableObject.CreateInstance<ProfileVariable>();
            profile.Init(entry.reportagem, entry.passaporte, entry.curriculo, entry.vagasDisponiveis, entry.paisDeOrigem);

            profiles2.Add(profile);
        }

		report.DefineLength (profiles2.Count);
		if (GameManager.instance.GetProfessorEdit ()) {
			Debug.Log ("true");
			canvasGameplay.SetActive (false);
			canvasConfig.SetActive (true);
		} else {
			Debug.Log ("false");
			canvasGameplay.SetActive (true);
			canvasConfig.SetActive (false);
		}

        Debug.Log(profiles2.Count);
	}

    #region USADO PARA CONTROLAR QUAL PROFILE ESTA ATIVO

    public void MakeDecision (int decision)
	{
		report.SetProfileIndex (i);
		report.SetProfileStatus (i, decision);
		//reporterText.text = "";
		//filesDocText.text = "";
		//pcText.text = "";

        //Atualiza a decisão no XML
        _xmlManager.entrevistadosDB.EntrevistadoList[i].aprovacao = (decision == 1 ? true : false);
        _xmlManager.Save();
	}

	public void NextInterview ()
	{
        if (i > profiles2.Count - 1) {
            print("passou do limite irmão");
            GameManager.instance.CallLoadScene(0);
        }
        else
            i++;
	}
    #endregion

    #region UI TV CONFIGURAÇÃO
    public void SetReporterText ()
	{
		reporterText.text = profiles2 [i].GetReporterText ();

	}
    #endregion

    #region UI PC CONFIGURAÇÃO

    public void GetPCCountryOriginText ()
	{
		pcText.text = profiles2 [i].GetPCOriginCountryText ();
	}

	public void GetPCAvailableJobsText ()
	{
		pcText.text = profiles2 [i].GetPCAvailableJobsText ();
	}
    #endregion

    #region UI FILES CONFIGURAÇÃO

    public void GetPassaportFileText ()
	{
		filesDocText.text = profiles2 [i].GetPassportText ();
	}

	public void GetCurriculumFileText ()
	{
		filesDocText.text = profiles2 [i].GetCurriculumText ();
	}
    #endregion

    #region CONTROLE_DE_TELAS
    // Feito atraves de botões no editor
    public void UIInitial ()
	{
		uiTV.SetActive (false);
		uiPC.SetActive (false);
		uiFiles.SetActive (false);
		uiInitial.SetActive (true);
	}

	public void UIFiles ()
	{
		uiTV.SetActive (false);
		uiPC.SetActive (false);
		uiFiles.SetActive (true);
		uiInitial.SetActive (false);
	}

	public void UIPC ()
	{
		uiTV.SetActive (false);
		uiPC.SetActive (true);
		uiFiles.SetActive (false);
		uiInitial.SetActive (false);
	}

	public void UITV ()
	{
		uiTV.SetActive (true);
		uiPC.SetActive (false);
		uiFiles.SetActive (false);
		uiInitial.SetActive (false);
	}
    #endregion 

    public void AddNovaEntrevista ()
	{
		ProfileVariable novoProfile = ScriptableObject.CreateInstance <ProfileVariable> ();
		novoProfile.Init 
			(
			inputReporterText.text, 
			inputPassaportText.text,
			inputCurriculumText.text,
			inputPcAvailableJobsText.text,
			inputPcOriginCountryText.text
		);
		report.DefineLength (profiles2.Count + 1);
		profiles2.Add (novoProfile);

        EntrevistadoEntry novoEntrevistado = new EntrevistadoEntry(novoProfile.passaportText, novoProfile.curriculumText, novoProfile.reporterText, novoProfile.pcOriginCountryText, novoProfile.pcAvailableJobsText, false);
        XMLManager.Instance.entrevistadosDB.EntrevistadoList.Add(novoEntrevistado);
        XMLManager.Instance.Save();
	}

}
