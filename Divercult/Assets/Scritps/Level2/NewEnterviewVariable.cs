﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu (menuName = "Scriptable/NewInterviewVariable")]
public class NewEnterviewVariable : ScriptableObject
{

	[Multiline (20)]
	public string passaportText;
	[Multiline (20)]
	public string curriculum;
	[Multiline (20)]
	public string reportText;
	[Multiline (20)]
	public string pcText;
	[Multiline (20)]
	public string availableJobs;

	public void SetpassaportText (string passText)
	{
		passaportText = passText;
	}

	public void SetPCTextCountry (string countryText)
	{
		pcText = countryText;
	}

	public void SetCurriculum (string curriculo)
	{
		curriculum = curriculo;
	}

	public void SetReportText (string repText)
	{
		reportText = repText;
	}

	public void SetAvailableJobs (string avaiJobs)
	{
		availableJobs = avaiJobs;
	}

	public string GetpassaportText ()
	{
		return passaportText;
	}

	public string GetSetcurriculum ()
	{
		return curriculum;
	}

	public string GetReportText ()
	{
		return reportText;
	}

	public string GetAvailableJobs ()
	{
		return availableJobs;
	}

	public string GetPCTextCountry ()
	{
		return pcText;
	}
		
}
