﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Scriptable/IntVariable")]
public class IntVariable : ScriptableObject
{

	[SerializeField]private int value;

	public void SetValue (int value)
	{
		this.value = value;
	}

	public int GetValue ()
	{
		return value;
	}

}
