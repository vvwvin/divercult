﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Scriptable/FloatVariable")]
public class FloatVariable : ScriptableObject
{

	[SerializeField]private float value;

	public void SetValue (float value)
	{
		this.value = value;
	}

	public float GetValue ()
	{
		return value;
	}

}
