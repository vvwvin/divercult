﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

	public static GameManager instance = null;


	private bool ProfessorEdit;

	void Awake ()
	{
		if (instance == null)
			instance = this;
		else
			Destroy (gameObject);

		DontDestroyOnLoad (gameObject);
	}

	public void SetProfessorEdit (bool isEditing)
	{
		ProfessorEdit = isEditing;
	}

	public bool GetProfessorEdit ()
	{
		return ProfessorEdit;
	}

	public void CallLoadScene (int cena)
	{
		SceneManager.LoadScene (cena);
	}

}
