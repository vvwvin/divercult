﻿using System;
using UnityEngine;

namespace Divercult.Level3
{
	[RequireComponent(typeof(CircleCollider2D))]
	public class PlayerObstacleRadius : MonoBehaviour
	{
		public static event Action<Obstacle, bool> OnTriggerObstacle = delegate { };

		private Obstacle obstacle = null;

		private void OnTriggerEnter2D(Collider2D collision)
		{
			if (collision.gameObject.CompareTag("Obstacle"))
			{
				obstacle = collision.gameObject.GetComponent<Obstacle>();

				OnTriggerObstacle(obstacle, true);
			}
		}

		private void OnTriggerExit2D(Collider2D collision)
		{
			if (collision.gameObject.CompareTag("Obstacle"))
			{
				OnTriggerObstacle(obstacle, false);
				obstacle = null;
			}
		}

		private void Update()
		{
			if (obstacle == null)
			{
				OnTriggerObstacle(obstacle, false);
			}
		}
	}
}