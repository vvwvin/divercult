﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover : MonoBehaviour {

    public bool isRunning;

    [SerializeField] private List<Transform> obstaclesPositions;
    [SerializeField] private GameObject obstaclesObject;
    
    private int ListIndex = 0;
	// Use this for initialization
	void Start () {
        isRunning = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (isRunning)
        {
            WalkToNextPosition();
        }
    }

    private void WalkToNextPosition ()
    {
        transform.position = Vector3.Lerp(transform.position, obstaclesPositions[ListIndex].position, 0.01f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isRunning = false;
        transform.position = obstaclesPositions[ListIndex].position;
        ListIndex++;
        obstaclesObject.SetActive(true);
    }
}
