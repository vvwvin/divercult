using UnityEngine;
using DG.Tweening;

namespace Divercult.Level3
{
	public class Obstacle : MonoBehaviour
	{
		public string Title { get { return title;} }
		public string Description { get { return description; } }
		public Sprite Sprite { get { return sprite; } }

		[SerializeField] private ResourceType type;
		[SerializeField, TextArea] private string description;
		[SerializeField, TextArea] private string title;
		[SerializeField] private Sprite sprite;

		public bool TryToDestroy(ResourceType resource)
		{
			if (resource == type)
			{
				transform.DOScale(0f, 0.2325f)
					.OnComplete(new TweenCallback(() =>
					{
						Destroy(gameObject);
					}));

				return true;
			}

			return false;
		}

	}
}