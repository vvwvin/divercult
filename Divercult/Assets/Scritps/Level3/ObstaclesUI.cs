﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObstaclesUI : MonoBehaviour {

    public Text description;
    public Text wCost, rCost, sCost;

    [SerializeField] private PlayerMover player;

    private ObstaclesManager OM;
    private ResourcesManager RM;

	// Use this for initialization
	void Start () {
        OM = ObstaclesManager.INSTANCE;
        RM = ResourcesManager.INSTANCE;
	}
	
	// Update is called once per frame
	void Update () {
        description.text = OM.obstaclesList[OM.listIndex].description;
        wCost.text = "Madeira: " + OM.obstaclesList[OM.listIndex].wCost;
        rCost.text = "Pedra: " + OM.obstaclesList[OM.listIndex].rCost;
        sCost.text = "Areia: " + OM.obstaclesList[OM.listIndex].sCost;
    }

    public void PayCost ()
    {
        RM.woodAmount -= OM.obstaclesList[OM.listIndex].wCost;
        RM.rockAmount -= OM.obstaclesList[OM.listIndex].rCost;
        RM.sandAmount -= OM.obstaclesList[OM.listIndex].sCost;
        OM.listIndex++;
        gameObject.SetActive(false);
        player.isRunning = true;
        

    }
}
