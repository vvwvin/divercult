﻿namespace Divercult.Level3
{
	public class Deck
	{
		public readonly DeckCard[] cards;
	}

	public class DeckCard
	{
		public readonly int id;
		public readonly string description;
		public readonly ResourceType resourceType;
	}
}