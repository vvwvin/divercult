using UnityEngine;

namespace Divercult.Level3
{
	public class CardHandle : MonoBehaviour
	{
		public Collider2D Collider { get { return collider; } }

		[SerializeField] private new Collider2D collider;

		private void Awake()
		{
			collider = GetComponent<Collider2D>();
			gameObject.SetActive(false);
		}

		public void Setup(Sprite sprite)
		{
			GetComponent<SpriteRenderer>().sprite = sprite;
		}
	}
}