﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesManager : MonoBehaviour {

    #region SINGLETON
    public static ObstaclesManager INSTANCE;

    private void Awake()
    {
        if (INSTANCE != this)
            Destroy(INSTANCE);
        if (INSTANCE == null)
            INSTANCE = this;
    }
    #endregion

    public List<Obstacle> obstaclesList;

    [System.NonSerialized] public int listIndex;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


}

[System.Serializable]
public struct Obstacle
{
    public int wCost, rCost, sCost;
    public string description;

}
