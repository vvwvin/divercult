using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using UnityEngine;

namespace Divercult.Level3
{
	public class Board : MonoBehaviour
	{
		[SerializeField] private Deck deck;
		[SerializeField] private CardController cardPrefab;
		[SerializeField] private Transform cardsParentTransform;

		private List<CardController> allCards;

		private void Start()
		{
			using (FileStream stream = new FileStream(Application.dataPath + "/StreamingAssets/XML/level3deck.xml", FileMode.Open))
			{
				XmlSerializer serializer = new XmlSerializer(typeof(Deck));
				deck = serializer.Deserialize(stream) as Deck;
			}

			allCards = new List<CardController>(deck.cards.Length);

			Sprite defaultImageSprite = Resources.Load<Sprite>("Level3Images/default");

			for (int i = 0; i < deck.cards.Length; i++)
			{
				Sprite deckCardImageSprite = Resources.Load<Sprite>("Level3Images/" + deck.cards[i].id);
				deckCardImageSprite = deckCardImageSprite ?? defaultImageSprite;

				CardController newCard = Instantiate(cardPrefab, cardsParentTransform);

				newCard.transform.localPosition = new Vector3(
					550f - 1100f / (deck.cards.Length - 1) * i,
					0f,
					0f
				);

				newCard.Setup(deckCardImageSprite, deck.cards[i].description, deck.cards[i].resourceType);

				allCards.Add(newCard);
			}
		}
	}
}