﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourcesUI : MonoBehaviour {

    [SerializeField] private Text woodAmount, rockAmount, sandAmount;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        woodAmount.text = ResourcesManager.INSTANCE.woodAmount.ToString ();
        rockAmount.text = ResourcesManager.INSTANCE.rockAmount.ToString();
        sandAmount.text = ResourcesManager.INSTANCE.sandAmount.ToString();
    }
}
