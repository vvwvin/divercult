using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Divercult.Level3
{
	public class UIPositionManager : MonoBehaviour
	{
		[SerializeField] private PlayerMovement playerMovement;
		[SerializeField] private HidableUI bottomPanel;
		[SerializeField] private HidableUI rightPanel;
		[SerializeField] private HidableUI leftPanel;
		[SerializeField] private PanelInfoDisplay rightDisplay;
		[SerializeField] private PanelInfoDisplay leftDisplay;

		private new Camera camera;

		private enum VerticalPosition { Bottom, Top };
		private enum HorizontalPosition { Left, Right };

		private void Awake()
		{
			camera = Camera.main;
		}

		private void Start()
		{
			PlayerObstacleRadius.OnTriggerObstacle += OnTriggerObstacleUI;
		}

		private void OnTriggerObstacleUI(Obstacle obstacle, bool isShown)
		{
			if (isShown)
			{
				rightDisplay.SetInfo(obstacle.Title, obstacle.Description, obstacle.Sprite);
				leftDisplay.SetInfo(obstacle.Title, obstacle.Description, obstacle.Sprite);

				switch (GetPlayerHorizontalPosition())
				{
					case HorizontalPosition.Left:
						rightPanel.Show();
						leftPanel.Hide();
						break;
					case HorizontalPosition.Right:
						leftPanel.Show();
						rightPanel.Hide();
						break;
				}
			}
			else
			{
				rightPanel.Hide();
				leftPanel.Hide();
			}
		}

		private void OnDisable()
		{
			PlayerObstacleRadius.OnTriggerObstacle -= OnTriggerObstacleUI;
		}

		private HorizontalPosition GetPlayerHorizontalPosition()
		{
			return GetRelativeScreenPosition(playerMovement.transform.position).x >= 0.5f ? HorizontalPosition.Right : HorizontalPosition.Left;
		}

		private Vector2 GetRelativeScreenPosition(Vector3 worldPosition)
		{
			Vector3 screenPositionInPixels = camera.WorldToScreenPoint(playerMovement.transform.position);
			Vector2 screenPosition = new Vector2(screenPositionInPixels.x /= Screen.width, screenPositionInPixels.y /= Screen.height);

			return screenPosition;
		}
	}
}