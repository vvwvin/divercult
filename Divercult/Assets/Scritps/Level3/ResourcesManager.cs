﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcesManager : MonoBehaviour {

    #region SINGLETON
    public static ResourcesManager INSTANCE;

    private void Awake()
    {
        if (INSTANCE != this)
            Destroy(INSTANCE);
        if (INSTANCE == null)
            INSTANCE = this;
    }
    #endregion

    public int woodAmount, rockAmount, sandAmount;

}
