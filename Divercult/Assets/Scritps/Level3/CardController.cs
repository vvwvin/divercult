﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

namespace Divercult.Level3
{
	//renomear para os recursos que vão ser usados no game
	[SerializeField] public enum ResourceType { Recurso1, Recurso2, Recurso3 }
	[SerializeField] public enum CardState { OnHand, BeingDragged, Used }

	public class CardController : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerClickHandler
	{
		[SerializeField] private Image image;
		[SerializeField] private Text description;
		[SerializeField] private ResourceType resource;
		[SerializeField] private CardState state = CardState.OnHand;
		[SerializeField] private new Collider2D collider;
		[SerializeField] private CardHandle cardHandlePrefab;
		[SerializeField] private ContactFilter2D filter;

		private Vector3 startLocalPosition;
		private CardHandle handle;
		private static new Camera camera;

		private void Awake()
		{
			camera = Camera.main;
		}

		public void Setup(Sprite sprite, string description, ResourceType resource)
		{
			image.sprite = sprite;
			this.description.text = description;
			this.resource = resource;

			startLocalPosition = transform.localPosition;

			handle = Instantiate(cardHandlePrefab);
			// usar esta função para carregar sprite do handle
			// handle.Setup(sprite);
		}

		public void OnDrag(PointerEventData eventData)
		{
			if (state == CardState.BeingDragged)
			{
				Cursor.visible = false;
				handle.transform.position += (Vector3)eventData.delta * 0.03f;
			}
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			if (eventData.button == PointerEventData.InputButton.Right)
				throw new System.NotImplementedException();
		}

		Collider2D[] contacts = new Collider2D[4];
		public void OnEndDrag(PointerEventData eventData)
		{
			if (state == CardState.BeingDragged)
			{
				bool obstacleDestroyed = false;
				Cursor.visible = true;
				transform.DOLocalMoveY(transform.localPosition.y - 64f, 0.2325f);

				int imageSideCollisionCount = handle.Collider.OverlapCollider(filter, contacts);

				foreach (Collider2D collider in contacts)
				{
					if (collider != null)
					{
						if (collider.gameObject.CompareTag("Obstacle"))
						{
							Obstacle obstacle = collider.gameObject.GetComponent<Obstacle>();

							obstacleDestroyed = obstacle.TryToDestroy(resource);

							if (obstacleDestroyed)
							{
								state = CardState.Used;

								handle.transform.DOScale(Vector3.zero, 0.1875f);

								transform.DOScale(Vector3.zero, 0.1875f)
								.OnComplete(new TweenCallback(() =>
								{
									Debug.Log(collider.gameObject.name + " used");
									Destroy(gameObject);
									return;
								}));
							}
						}
					}
				}

				if (!obstacleDestroyed)
				{
					handle.transform.DOMove(handlePosition, 0.2375f)
					.OnComplete(new TweenCallback(() =>
					{
						Debug.Log("voltaneo");
						handle.gameObject.SetActive(false);
					}));

					transform.DOLocalMove(startLocalPosition, 0.1875f)
					.OnComplete(new TweenCallback(() =>
					{
						state = CardState.OnHand;
					}));
				}
			}
		}

		Vector3 handlePosition = Vector3.zero;
		public void OnBeginDrag(PointerEventData eventData)
		{
			if (state == CardState.OnHand)
			{
				state = CardState.BeingDragged;
				transform.DOLocalMoveY(transform.localPosition.y + 64f, 0.2325f);

				handlePosition = camera.ScreenToWorldPoint(Input.mousePosition);
				handlePosition.y += 1.5f;
				handlePosition.z = handle.transform.position.z;

				handle.transform.position = handlePosition;

				handle.gameObject.SetActive(true);
			}
		}
	}
}