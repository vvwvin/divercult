﻿using UnityEngine;
using UnityEngine.UI;

namespace Divercult.Level3
{
	public class PanelInfoDisplay : MonoBehaviour
	{
		[SerializeField] private Text title;
		[SerializeField] private Text body;
		[SerializeField] private Image image;

		public void SetInfo(string titleText, string bodyText, Sprite imageSprite)
		{
			title.text = titleText;
			body.text = bodyText;
			image.sprite = imageSprite;
		}
	}
}