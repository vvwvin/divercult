using UnityEngine;

namespace Divercult.Level3
{
	[RequireComponent(typeof(Rigidbody2D))]
	public class PlayerMovement : MonoBehaviour
	{
		[SerializeField] private float moveSpeed = 5f;

		private Rigidbody2D rigidBody2D;

		public bool movementAllowed = true;

		private void Awake()
		{
			rigidBody2D = GetComponent<Rigidbody2D>();
		}

		private void Update()
		{
			float moveHorizontal = Input.GetAxis("Horizontal") * moveSpeed;
			float moveVertical = Input.GetAxis("Vertical") * moveSpeed;
			Vector3 newPosition = transform.position + new Vector3(moveHorizontal, moveVertical, 0f) * Time.deltaTime;

			rigidBody2D.MovePosition(newPosition);
		}
	}
}